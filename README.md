# Luckyadmin v1.0

#### 介绍
LuckyAdmin是一款基于ThinkPHP5.0 + layui2.5.4 + ECharts + Mysql+Redis开发的后台管理框架，集成了一般应用所必须的基础性功能，为开发者节省大量的时间。
演示地址： [http://demo.jackhhy.cn](http://demo.jackhhy.cn)

#### 系统架构
1. layui2.5.4
2. Thinkphp5.0
3. Mysql 5.6
4. PHP-V 7.0

#### 安装教程

1. 点击下载压缩包或者 git clone https://gitee.com/luckygyl/LuckyAdmin.git 下载代码
2. 切换到项目目录下 执行：composer update 或者 composer install 更新框架 Vendor下文件
3. 导入 public/Databack/xxx.sql 文件到数据库
4. 修改 application/database.php 数据库文件配置选项
5. 访问： http://yourdomain/admin

#### 项目截图

![输入图片说明](https://images.gitee.com/uploads/images/2019/0717/170019_285fe44d_1513275.png "QQ截图20190717165234.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0717/170038_0e0bf860_1513275.png "QQ截图20190717165412.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0717/170030_a20a7ae7_1513275.png "QQ截图20190717165335.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0717/170125_d4ec8502_1513275.png "QQ截图20190717165455.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0717/170229_9569e09d_1513275.png "QQ截图20190717165518.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0717/170241_daaced9e_1513275.png "QQ截图20190717165543.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0717/170254_9266648f_1513275.png "QQ截图20190717165554.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0717/170309_dd0608d4_1513275.png "QQ截图20190717165750.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0717/170322_1e2d175e_1513275.png "QQ截图20190717165840.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0717/173350_8335965c_1513275.png "QQ截图20190717165808.png")
![输入图片说明](https://images.gitee.com/uploads/images/2019/0717/170333_48fd52c9_1513275.png "QQ截图20190717165912.png")


#### 特别鸣谢

1. [ok-admin](https://gitee.com/bobi1234/ok-admin/tree/v2.0/)
2. [layui](http://www.layui.com)
3. [thinkphp5.0](http://www.thinkphp.cn)


#### 往期项目

1. [图标选择器](https://gitee.com/luckygyl/iconFonts)
