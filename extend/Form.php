<?php
    /**
     * Created by PhpStorm.
     * User: jackhhy
     * Date: 2019/7/10-11:00
     * Link: https://gitee.com/luckygyl/lucky_admin
     * FileName: Form.php
     * Keys: ctrl+alt+L/ctrl+s(代码格式化) ctrl+J(代码提示) ctrl+R(替换)ALT+INSERT(生成代码(如GET,SET方法,构造函数等) , 光标在类中才生效)
     * CTRL+ALT+O (优化导入的类和包 需要配置) SHIFT+F2(高亮错误或警告快速定位错误)
     * CTRL+SHIFT+Z(代码向前) CTRL+SHIFT+/ (块状注释) ctrl+shift+enter(智能完善代码 如if())
     */
class Form {

    /**
     * @param string $type
     * @param $name
     * @param $value
     * @param $label
     * @param string $placeholder
     * @param string $verify
     * @param string $description
     * @param string $color
     * @param string $in_bloc
     * @param bool $is_readonly
     * @return string
     * @author: Jackhhy <jackhhy520@qq.com>
     * @name: Input
     * @describe:单行文本
     */
    static public function Input($type='text',$name, $value, $label, $placeholder='',$verify='',$description='',$color='text-warning',$in_bloc='block',$nei='',$class='',$id="",$style='',$is_readonly = false){
        if(empty($nei)){
            $string=' <div class="layui-form-item '.$class.'"  style="'.$style.'" >
                        <label class="layui-form-label">'.$label.'：</label>
                        <div class="layui-input-'.$in_bloc.' input-custom-width">
                            <input type="'.$type.'" name="'.$name.'"  value="'.$value.'"  id="'.$id.'" placeholder="'.$placeholder.'" lay-verify="'.$verify.'" lay-verType="tips" lay-reqText="'.$placeholder.'"  autocomplete="off" '.($is_readonly ? 'readonly' : '').' class="layui-input">
                        </div>';
            if($description) {
                $string .= '<div class="layui-form-mid layui-word-aux layui-text '.$color.'" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                           '.$description.'</div>';
            }
            $string .= '</div>';
        }else{ //内联单行排列
            $string=' <label class="layui-form-label">'.$label.'：</label>
                        <div class="layui-input-'.$in_bloc.' input-custom-width">
                            <input type="'.$type.'" name="'.$name.'"  value="'.$value.'"   placeholder="'.$placeholder.'" lay-verify="'.$verify.'" lay-verType="tips" lay-reqText="'.$placeholder.'"  autocomplete="off" '.($is_readonly ? 'readonly' : '').' class="layui-input">
                        </div>';

        }

        return $string;
    }



    /**
     * @param $name
     * @param $lable
     * @param $pla
     * @return string
     * @author: Jackhhy <jackhhy520@qq.com>
     * @name: Search
     * @describe:搜索
     */
    static public function Search($name,$lable,$pla){
        $string='  <!--搜索开始-->
            <div class="layui-card">
                <div class="layui-card-body layui-row ">
                    <form action="" class="layui-form" method="get" >
                        <div class="layui-form-item">
                            <div class="layui-inline">
                                <label class="layui-form-label">'.$lable.'：</label>
                                <div class="layui-input-inline">
                                    <input name="'.$name.'" class="layui-input" value="" type="text" placeholder="'.$pla.'">
                                </div>
                            </div>

                            <div class="layui-inline" style="padding-left: 20px;">
                                <button class="layui-btn icon-btn" lay-filter="search" lay-submit="">
                                    <i class="layui-icon"></i>搜索
                                </button>
                                 <button class="layui-btn icon-btn layui-btn-normal"  onclick="javascript:window.location.reload();" >
                                   <i class="layui-icon layui-icon-refresh"></i> 重置
                                </button>
                            </div>

                        </div>

                    </form >
                </div>

            </div>
            <!--搜索结束-->';
        return $string;
    }


    /**
     * @param $name
     * @param $value
     * @param $label
     * @param string $description
     * @param string $placeholder
     * @param string $verify
     * @param string $in_bloc
     * @return string
     * @author: Jackhhy <jackhhy520@qq.com>
     * @name: textarea
     * @describe:多行文本
     */
    static public function Textarea($name, $value, $label, $description='',$placeholder='',$verify='',$color='text-warning',$in_bloc='block') {
        $string = '<div class="layui-form-item"><label class="layui-form-label">'.$label.'：</label>';
        $string .= '<div class="layui-input-'.$in_bloc.' input-custom-width"><textarea name="'.$name.'" lay-verify="'.$verify.'" lay-reqText="'.$placeholder.'" lay-verType="tips" autocomplete="off" placeholder="'.$placeholder.'" class="layui-textarea">'.$value.'</textarea></div>';
        if($description) {
            $string .= '<div class="layui-form-mid layui-word-aux layui-text'.$color.'" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                           '.$description.'</div>';
        }
        $string .= '</div>';
        return $string;
    }



    /**
     * @param $name
     * @param $value
     * @param $label
     * @param string $placeholder
     * @param string $verify
     * @param string $btn
     * @return string
     * @author: Jackhhy <jackhhy520@qq.com>
     * @name: Upavatar
     * @describe:上传缩略图
     */
    static public function Upthumb($name, $value,$label,$placeholder='',$verify='',$btn='上传图片'){
        $string='  <div class="layui-form-item">
                        <label class="layui-form-label">'.$label.'：</label>
                        <div class="layui-input-inline">
                            <input name="'.$name.'" lay-verify="'.$verify.'" lay-verType="tips" lay-reqText="'.$placeholder.'" id="LAY_avatarSrc" placeholder="'.$placeholder.'" value="'.$value.'"  readonly class="layui-input">
                        </div>
                        <div class="layui-input-inline layui-btn-container" style="width: auto;">
                            <button type="button" class="layui-btn layui-btn-primary" id="LAY_avatarUpload">
                                <i class="layui-icon">&#xe67c;</i>'.$btn.'
                            </button>
                        </div>
                    </div>

                    <div class="layui-form-item">
                        <label class="layui-form-label">图片显示：</label>
                        <div class="layui-input-inline">
                            <div class="layui-upload">
                                <div class="layui-upload-list">
                                    <img class="layui-upload-img" src="'.$value.'" id="demo1">
                                </div>
                            </div>
                        </div>
                    </div>';
        return $string;
    }


    /**
     * @param $name
     * @param $value
     * @param $label
     * @param string $description
     * @param string $placeholder
     * @param string $verify
     * @param string $id
     * @return string
     * @author: Jackhhy <jackhhy520@qq.com>
     * @name: Layedit
     * @describe:layui编辑器
     */
    static public function Layedit($name, $value, $label, $description='',$placeholder='',$verify='', $id = 'layedit') {
        $string = '<div class="layui-form-item"><label class="layui-form-label">'.$label.'：</label>';
        $string .= '<div class="layui-input-block"><textarea name="'.$name.'" lay-verify="'.$verify.'" lay-verType="tips" autocomplete="off" lay-reqText="'.$placeholder.'" placeholder="'.$placeholder.'" class="layui-textarea layui-hide" id="'.$id.'">'.htmlentities($value).'</textarea></div>';
        if($description) {
            $string .= '<div class="layui-form-mid layui-word-aux">'.$description.'</div>';
        }
        $string .= '</div>';
        return $string;
    }

    /**
     * @param $name
     * @param string $value
     * @param $label
     * @param string $description
     * @param string $in_bloc
     * @return string
     * @author: Jackhhy <jackhhy520@qq.com>
     * @name: Enabled
     * @describe:开启/关闭
     */
    static public function Enabled($name, $value = 'on', $label, $description='',$in_bloc='block') {
        $string = '<div class="layui-form-item"><label class="layui-form-label">'.$label.'：</label><div class="layui-input-'.$in_bloc.' input-custom-width">';
        $checked = ($value == 'on') ? ' checked' : '';
        $string .= '<input type="checkbox"  name="'.$name.'" '.$checked.' lay-skin="switch" title="开关">';
        $string .='</div><div class="layui-form-mid layui-word-aux">'.$description.'</div></div>';
        return $string;
    }


    /**
     * @param string $filter
     * @param string $btn
     * @return string
     * @author: Jackhhy <jackhhy520@qq.com>
     * @name: Submit
     * @describe:提交
     */
    static public function Submit($filter='add',$btn='保存提交'){
        $string='<div class="layui-form-item layui-layout-admin " style="width: 100%">
                        <div class="layui-input-block">
                            <div class="layui-footer" style="left: 0px;text-align: center;">
                                <button class="layui-btn" lay-submit="" lay-filter="'.$filter.'">'.$btn.'</button>
                                <button type="reset" class="layui-btn layui-btn-primary">重置</button>
                            </div>
                        </div>
                    </div>';
        return $string;
    }

    /**
     * @param $name
     * @param $value
     * @param $label
     * @param string $description
     * @param array $items
     * @param string $filter
     * @param string $in_bloc
     * @return bool|string
     * @author: Jackhhy <jackhhy520@qq.com>
     * @name: Radio
     * @describe:单选框
     */
    static public function Radio($name, $value, $label, $items = array(),$description='', $filter='',$in_bloc='block',$nei='') {
        if(!is_array($items) || empty($items)) return false;
        if(empty($nei)){
            $string = '<div class="layui-form-item"><label class="layui-form-label">'.$label.'：</label><div class="layui-input-'.$in_bloc.' input-custom-width">';
            foreach( $items as $key => $item ) {
                $checked = ($value == $key) ? ' checked' : '';
                $string .= '<input type="radio" name="'.$name.'" value="'.$key.'" title="'.$item.'" lay-filter="'.$filter.'" '.$checked.'>';
            }
            $string .= '</div><div class="layui-form-mid layui-word-aux">'.$description.'</div></div>';
        }else{
            $string = '<label class="layui-form-label">'.$label.'：</label><div class="layui-input-'.$in_bloc.' input-custom-width">';
            foreach( $items as $key => $item ) {
                $checked = ($value == $key) ? ' checked' : '';
                $string .= '<input type="radio" name="'.$name.'" value="'.$key.'" title="'.$item.'" lay-filter="'.$filter.'" '.$checked.'>';
            }
            $string .= '</div><div class="layui-form-mid layui-word-aux">'.$description.'</div>';
        }
        return $string;
    }

    /**
     * @param $name
     * @param $value
     * @param $label
     * @param string $description
     * @param array $items
     * @return bool|string
     * @author: Jackhhy <jackhhy520@qq.com>
     * @name: Checkbox
     * @describe:多选框
     */
    static public function Checkbox($name, $value, $label, $items = array(),$description='', $in_bloc='block') {
        if(!is_array($items) || empty($items)) return false;
        $string = '<div class="layui-form-item"><label class="layui-form-label">'.$label.'：</label><div class="layui-input-'.$in_bloc.' input-custom-width">';
        $value = explode(',', $value);
        foreach( $items as $key => $item ) {
            $checked = (in_array($key, $value)) ? ' checked' : '';
            $string .= '<input type="checkbox" name="'.$name.'['.$key.']" value="'.$key.'" title="'.$item.'" '.$checked.'>';
        }
        $string .= '</div><div class="layui-form-mid layui-word-aux">'.$description.'</div></div>';
        return $string;
    }

    /**
     * @param $name
     * @param $value
     * @param $label
     * @param string $description
     * @param array $options
     * @param string $verify
     * @return bool|string
     * @author: Jackhhy <jackhhy520@qq.com>
     * @name: select
     * @describe:下拉框
     */
    static public function Select($name, $value, $label, $description='', $options = array(), $verify='' ) {
        if(!is_array($options) || empty($options)) return false;
        $string = '<div class="layui-form-item"><label class="layui-form-label">'.$label.'：</label><div class="layui-input-inline input-custom-width"><select name="'.$name.'" lay-verify="'.$verify.'" lay-verType="tips">';
        $string .= '<option value="" >请选择</option>';
        foreach( $options as $key => $option ) {
            $selected = ($value == $key) ? true : false;
            $string .= '<option value="'.$key.'" '.($selected ? ' selected="" ' : '').' >'.$option.'</option>';
        }
        $string .= '</select></div><div class="layui-form-mid layui-word-aux">'.$description.'</div></div>';
        return $string;
    }



    /**
     * @param $name
     * @param $value
     * @param $label
     * @param string $description
     * @param string $id
     * @return string
     * @author: Jackhhy <jackhhy520@qq.com>
     * @name: Images
     * @describe:图集上传
     */
    static public function Images($name, $value, $label, $description='',$id = 'images') {
        $string = '<div class="layui-form-item"><label class="layui-form-label">'.$label.'</label><div class="layui-input-block images-block-container">';
        if(is_array($value)){
            foreach ($value as $k => $v) {
                $string .= '<div class="image-block"><input type="hidden" name="images['.$k.']" value="'.$v.'" class="images-input"><img class="img" src="'.$v.'"><div class="image-block-mask"><span class="del_btn"><i class="layui-icon">&#x1006;</i></span><a class="layui-btn set-index">设为主图</a></div></div>';
            }
        }
        $string .= '<div class="image-add-blcok"><input type="file" name="file[]" lay-method="post" lay-type="images"  lay-title="" class="layui-upload-file" id="'.$id.'" multiple="multiple"></div></div>';
        if($description) {
            $string .= '<div class="layui-form-mid layui-word-aux">'.$description.'</div>';
        }
        $string .= '</div>';
        return $string;
    }

}
