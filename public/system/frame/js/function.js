/**
 * 表格搜索
 * @param tableid
 * @param data
 * @returns {boolean}
 * @constructor
 */
function CreateSearch(tableid,data){
    if (data==""||data==undefined){
        data=[];
    }
    var index=layer.msg("查询中，请稍后...",{icon:16,time:false,shade: 0.3,anim:4});
    setTimeout(function () {
        //执行重载
        layui.table.reload(tableid, {
            page: {
                curr: 1 //重新从第 1 页开始
            }
            ,where:data
        }, 'data');
        layer.close(index);
    },500);

}

/**
 * 关闭并刷新父页面
 * @param tableid
 */
function closeLayerReload(tableid){
    var index = parent.layer.getFrameIndex(window.name);
    parent.layer.close(index);
    parent.layui.table.reload(tableid,'data');
}


/**
 * 关闭父级页面
 */
function closeLayer(){
    var index = parent.layer.getFrameIndex(window.name);
    parent.layer.close(index);
}


function closeFa() {
    parent.location.reload(); //刷新父页面
    var index = parent.layer.getFrameIndex(window.name);
    parent.layer.close(index);
}


/**
 * 刷新表格
 * @param tableid
 * @constructor
 */
//刷新事件
function CreateReload(tableid){
    //执行重载
    layui.table.reload(tableid,{where:{}},'data');
}


/**
 * 表单提交数据
 * @param url
 * @param data
 * @param tablid
 * @param is_close
 * @constructor
 */
function FormSubmit(url,data,tablid=0,is_close=0) {
    $.ajax({
        url:url,
        type:'post',
        data:data,
        error: function(error) {
            layer.msg("服务器错误或超时");
            return false;
        },
        beforeSend:function(){
            layer.load(2);
        },
        success:function(data)
        {
            if (data.code==1) {
                if(is_close!=0 && tablid!=0){
                    layer.msg(data.msg,{icon: 1, time: 1500,shade:0.3, anim: 4});
                    setTimeout(function(){
                        closeLayerReload(tablid); //重载父页面表格
                    },500);

                }else if(is_close!=0){
                    layer.msg(data.msg,{icon: 1, time: 1500,shade:0.3, anim: 4});
                    setTimeout(function(){
                        closeLayer();//不重载父页面表格
                    },500);

                }else if (tablid!=0){
                    layer.msg(data.msg,{icon: 1, time: 1500,shade:0.3, anim: 4});
                    setTimeout(function(){
                        CreateReload(tablid); //重载当前页面的表格
                    },500);

                } else {
                    layer.msg(data.msg,{icon: 1, time: 1500,shade:0.3, anim: 4},function () {
                        //location.reload();
                    });
                }
            }else{
                layer.msg(data.msg,{icon:15,time:1500,shade:0.3,anim:4});
            }
        },
        complete:function(){
            layer.closeAll('loading');
        }
    });
}

/**
 * 创建弹出窗
 * @param title
 * @param w
 * @param h
 * @param url
 * @param tableid
 * @constructor
 */
function CreateForm(title,w,h,url,tableid=0,close=0){
    title?title:'管理界面';
    w?w:'40%';
    h?h:'60%';
    layer.open({
        title:title,
        type: 2,
        area: [w, h],
        offset:'auto',
        maxmin : true,
        skin:'layui-layer-molv',
        shade: 0.5,
        content: url,
        success:function(){
            setTimeout(function(){
                layui.layer.tips('点击此处返回列表', '.layui-layer-setwin .layui-layer-close', {
                    tips: 3,time:1000
                });
            },500)
        },
        end: function () {
           if (tableid==0){
             console.log(tableid);
           }else if(close!=0){
              location.reload();
            }
           else {
               setTimeout(function(){
                   CreateReload(tableid);//重载表格
               },500);
           }
        }
    });
}



/**
 * 恢复数据
 * @param NodeID
 * @param ids
 * @param URL
 * @param tableid
 * @returns {boolean}
 */
function recycle_data(ids,URL,tableid)
{
    if($.isArray(ids))
    {
        ids = ids.join(',');
    }
    if (ids=="")
    {
        layer.msg("没选择任何数据",{time:1500});return false;
    }
    layer.confirm('确定恢复吗？', function(index){
        layer.close(index);
        $.ajax({
            beforeSend:function(){
                layer.load(2);
            },
            url: URL,
            type: "POST",
            async: true,
            dataType: "json",
            data:{
                ids: ids,
            },
            error: function(error) {
                layer.msg("服务器错误或超时");
                return false;
            },
            success: function(data) {
                if (data.code==1) {
                    layer.msg(data.msg,{icon:1,time:1500,shade:0.3,anim:4});
                    setTimeout(function(){
                        CreateReload(tableid); //重载表格数据
                    },500);
                }else{
                    layer.msg(data.msg,{icon:15,time:1500,shade:0.3},function () {
                        CreateReload(tableid);
                    });
                }
            },
            complete:function(){
                layer.closeAll('loading');
            }
        });
    });
}



/**
 * 删除数据
 * @param NodeID
 * @param ids
 * @param URL
 * @param tableid
 * @returns {boolean}
 */
function delete_data(ids,URL,tableid,msg='确定删除吗？')
{
    if($.isArray(ids))
    {
        ids = ids.join(',');
    }
    if (ids=="")
    {
        layer.msg("没选择任何数据",{time:1500});return false;
    }
    layer.confirm(msg, function(index){
        layer.close(index);
        $.ajax({
            beforeSend:function(){
                layer.load(2);
            },
            url: URL,
            type: "POST",
            async: true,
            dataType: "json",
            data:{
                ids: ids,
            },
            error: function(error) {
                layer.msg("服务器错误或超时");
                return false;
            },
            success: function(data) {
                if (data.code==1) {
                    layer.msg(data.msg,{icon:1,time:1500,shade:0.3,anim:4});
                    setTimeout(function(){
                        CreateReload(tableid); //重载表格数据
                    },500);
                }else{
                    layer.msg(data.msg,{icon:15,time:1500,shade:0.3},function () {
                        CreateReload(tableid);
                    });
                }
            },
            complete:function(){
                layer.closeAll('loading');
            }
        });
    });
}


/**
 * 异步数据提交改变表字段状态
 * @param tablename
 * @param id
 * @param field
 * @param status
 */
function change_status(table_id,tablename,id,field,status)//分别对应表id表明,主键,字段名,状态值
{
    $.ajax({
        url:"/admin/Common/ChangeStatus",
        type:'post',
        async: true,
        dataType: "json",
        data:{
            id:id,
            table:tablename,
            field:field,
            status:status
        },
        error: function(error) {
            layer.msg("服务器错误或超时");
            return false;
        },
        beforeSend:function(){
            layer.load(2);
        },
        success:function(res)
        {
            //CreateReload(table_id);
            if (res.code==200) {
                layer.msg(res.msg,{icon:1,time:800,shade:0.3,anim:4},function () {
                    CreateReload(table_id);
                });
            }else{
                layer.msg(res.msg,{icon:15,time:800,shade:0.3,anim:4},function () {
                    CreateReload(table_id);
                });
            }
        },
        complete:function(){
            layer.closeAll('loading');
        }
    });
}


/**
 * 生成树结构
 * @param rows
 * @returns {Array}
 */
function createTree(rows)
{
    var nodes = [];
    for (var i = 0; i < rows.length; i++) {
        var row = rows[i];
        if (!exists(rows, row.pid)) {
            nodes.push({
                id: row.id,
                name: row.name,
                value:row.value,
                children:row.children
            });
        }
    }
    var toDo = [];

    for (var i = 0; i < nodes.length; i++) {
        toDo.push(nodes[i]);
    }

    for (var i = 0; i < nodes.length; i++) {
        toDo.push(nodes[i]);
    }
    while (toDo.length) {
        var node = toDo.shift();   // the parent node
        // get the children nodes
        for (var i = 0; i < rows.length; i++) {
            var row = rows[i];
            if (row.pId == node.id) {
                var child = {id: row.id, name: row.name, pId: node.id,value:row.value,children:row.children};
                if (node.children) {
                    node.children.push(child);
                } else {
                    node.children = [child];
                }
                toDo.push(child);
            }
        }
    }
    return nodes;//layui nodes对象

}

/**
 * 判断是否存在父节点
 * @param rows
 * @param pId
 * @returns {boolean}
 */
function exists(rows, pId) //
{
    for (var i = 0; i < rows.length; i++) {
        if (rows[i].id == pId) return true;
    }
    return false;
}



