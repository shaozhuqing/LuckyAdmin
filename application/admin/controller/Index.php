<?php
    /**
     * Created by PhpStorm.
     * User: jackhhy
     * Date: 2019/6/26-14:31
     * Link: https://gitee.com/luckygyl/lucky_admin
     * FileName: Index.php
     * Keys: ctrl+alt+L/ctrl+s(代码格式化) ctrl+J(代码提示) ctrl+R(替换)ALT+INSERT(生成代码(如GET,SET方法,构造函数等) , 光标在类中才生效)
     * CTRL+ALT+O (优化导入的类和包 需要配置) SHIFT+F2(高亮错误或警告快速定位错误)
     * CTRL+SHIFT+Z(代码向前) CTRL+SHIFT+/ (块状注释) ctrl+shift+enter(智能完善代码 如if())
     */

    namespace app\admin\controller;

    use service\CacheService;
    use service\UtilService;
    use think\Request;

    class Index extends SystemBase
    {

        protected function _initialize()
        {
            parent::_initialize(); // TODO: Change the autogenerated stub

        }



        /**
         * @param Request $request
         * @return mixed
         * @author: Jackhhy <jackhhy520@qq.com>
         * @name: index
         * @describe:
         */
        public function index(Request $request)
        {
            if(CacheService::get(self::$admin_info['id'].self::$admin_info['username'])) { //有锁屏进入锁屏页面
                $this->redirect(url("index/lock"));
                exit;
            }


            return $this->fetch();

        }



        /**
         * @return mixed
         * @author: Jackhhy <jackhhy520@qq.com>
         * @name: home
         * @describe:
         */
        public function home()
        {

            $role_id = self::$admin_info['role_id'];
            isset($role_id) ? $role_id : 0;
            $res = db("admin_role")->where("id", $role_id)->value("rule");

            if(empty($res)) {
                return $this->fetch("public/no_rule");
            }
            else {
                $log = db("admin_log")->order("create_time desc")->limit(15)->select();
                $this->assign("log", $log);

                return $this->fetch();
            }
        }

   /**
         * @return mixed
         * @author: Jackhhy <jackhhy520@qq.com>
         * @name: weather
         * @describe:天气显示
         */
        public function weather(){
            return $this->fetch();
        }

        /**
         * @author: Jackhhy <jackhhy520@qq.com>
         * @name: lock_pass
         * @describe:设置锁屏密码
         */
        public function lock_pass()
        {
            if($this->request->isPost()) {
                $Pass = $this->request->post("pass");
                if(empty($Pass)) {
                    $this->error("锁屏密码不能为空");
                }
                CacheService::set(self::$admin_info['id'].self::$admin_info['username'], $Pass);//保存锁屏密码
                $this->success("锁屏成功");
            }
        }


        /**
         * @return mixed
         * @author: Jackhhy <jackhhy520@qq.com>
         * @name: lock
         * @describe:锁屏页面
         */
        public function lock()
        {
            if(!CacheService::get(self::$admin_info['id'].self::$admin_info['username'])) { //解锁完毕
                $this->redirect(url("index/index"));
                exit;
            }
            return $this->fetch();
        }



        /**
         * @author: Jackhhy <jackhhy520@qq.com>
         * @name: pass_lock
         * @describe:使用登录密码解锁
         */
        public function pass_lock()
        {
            if($this->request->isPost()) {
                $Pass = $this->request->post("password");
                if(empty($Pass)) {
                    $this->error("管理员登录密码不能为空");
                }

                if(UtilService::think_decrypt(self::$admin_info['password'])==$Pass) {
                    CacheService::rm(self::$admin_info['id'].self::$admin_info['username']); //清除解锁密码
                    $this->success("解锁成功");
                }
                else {
                    $this->error("管理登录密码错误！");
                }
            }
        }

        /**
         * @author: Jackhhy <jackhhy520@qq.com>
         * @name: unlock
         * @describe:解锁
         */
        public function unlock()
        {
            if($this->request->isPost()) {
                $Pass = $this->request->post("pass");
                if(empty($Pass)) {
                    $this->error("锁屏密码不能为空");
                }

                $lockpass = CacheService::get(self::$admin_info['id'].self::$admin_info['username']);
                if($lockpass==$Pass) {
                    CacheService::rm(self::$admin_info['id'].self::$admin_info['username']); //清除解锁密码
                    $this->success("解锁成功");
                }
                else {
                    $this->error("锁屏密码错误！");
                }
            }
        }



    }