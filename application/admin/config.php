<?php
    /**
     * Created by PhpStorm.
     * User: jackhhy
     * Date: 2019/6/26-14:14
     * Link: https://gitee.com/luckygyl/lucky_admin
     * FileName: config.php
     * Keys: ctrl+alt+L/ctrl+s(代码格式化) ctrl+J(代码提示) ctrl+R(替换)ALT+INSERT(生成代码(如GET,SET方法,构造函数等) , 光标在类中才生效)
     * CTRL+ALT+O (优化导入的类和包 需要配置) SHIFT+F2(高亮错误或警告快速定位错误)
     * CTRL+SHIFT+Z(代码向前) CTRL+SHIFT+/ (块状注释) ctrl+shift+enter(智能完善代码 如if())
     */

    use app\admin\controller\AdminException;

    return [
        'session'                => [
            // SESSION 前缀
            'prefix'         => 'admin',
            // 驱动方式 支持redis memcache memcached
            'type'           => '',
            // 是否自动开启 SESSION
            'auto_start'     => true,
        ],
        'app_debug'              => true,
        // 应用Trace
        'app_trace'              => false,
        'show_error_msg'         => true,
       // 'exception_handle' => app\admin\controller\AdminException::class,

        //'empty_controller' =>'Index',

        // 视图输出字符串内容替换
        'view_replace_str'       => [
            '{__PUBLIC_PATH}' =>  PUBILC_PATH,                 //public 目录
            '{__VIEW_PATH}'   =>  VIEW_PATH,
            '{__STATIC_PATH}' =>  PUBILC_PATH.'static/',       //全局静态目录
            '{__ADMIN_PATH}'  =>  PUBILC_PATH.'system/',       //后台目录
            '{__FRAME_PATH}'  =>  PUBILC_PATH.'system/frame/', //后台框架
            '{__MODULE_PATH}' =>  PUBILC_PATH.'system/module/',//后台模块
            '{__PLUG_PATH}' =>  PUBILC_PATH.'system/plug/',//后台插件
        ],




    ];