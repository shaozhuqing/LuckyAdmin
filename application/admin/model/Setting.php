<?php
    /**
     * Created by PhpStorm.
     * User: jackhhy
     * Date: 2019/7/4-15:39
     * Link: https://gitee.com/luckygyl/lucky_admin
     * FileName: Setting.php
     * Keys: ctrl+alt+L/ctrl+s(代码格式化) ctrl+J(代码提示) ctrl+R(替换)ALT+INSERT(生成代码(如GET,SET方法,构造函数等) , 光标在类中才生效)
     * CTRL+ALT+O (优化导入的类和包 需要配置) SHIFT+F2(高亮错误或警告快速定位错误)
     * CTRL+SHIFT+Z(代码向前) CTRL+SHIFT+/ (块状注释) ctrl+shift+enter(智能完善代码 如if())
     */

    namespace app\admin\model;


    use think\Model;

    class Setting extends Model
    {

        /**
         * @return array
         * @author: Jackhhy <jackhhy520@qq.com>
         * @name: getSetting
         * @describe:获取设置选项
         */
        public function getSetting()
        {
            $data = $this->column('key,value');

            $data['email'] = json_decode($data['email'], true); //邮箱设置
            $data['safe']  = json_decode($data['safe'], true); //安全設置

            return $data;
        }


    }