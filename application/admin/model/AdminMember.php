<?php
    /**
     * Created by PhpStorm.
     * User: jackhhy
     * Date: 2019/7/5-15:14
     * Link: https://gitee.com/luckygyl/lucky_admin
     * FileName: AdminMember.php
     * Keys: ctrl+alt+L/ctrl+s(代码格式化) ctrl+J(代码提示) ctrl+R(替换)ALT+INSERT(生成代码(如GET,SET方法,构造函数等) , 光标在类中才生效)
     * CTRL+ALT+O (优化导入的类和包 需要配置) SHIFT+F2(高亮错误或警告快速定位错误)
     * CTRL+SHIFT+Z(代码向前) CTRL+SHIFT+/ (块状注释) ctrl+shift+enter(智能完善代码 如if())
     */

    namespace app\admin\model;


    use app\common\model\ModelBase;
    use service\JsonService;
    use service\TreeService;
    use service\UtilService;
    use think\Exception;

    class AdminMember extends ModelBase
    {

        protected $update = ["update_time"];


        /**
         * @return mixed
         * @author: Jackhhy
         * @name: setUpdateTimeAttr
         * @describe:
         */
        protected function setUpdateTimeAttr()
        {
            return time();
        }


        /**
         * @param $data
         * @return false|int
         * @author: Jackhhy <jackhhy520@qq.com>
         * @name: addData
         * @describe:添加数据
         */
        public function addData($data)
        {
            return $this->isUpdate(false)->allowField(true)->save($data);
        }


        /**
         * @param $data
         * @return false|int
         * @author: Jackhhy <jackhhy520@qq.com>
         * @name: editData
         * @describe:编辑数据
         */
        public function editData($data)
        {
            return $this->isUpdate(true)->allowField(true)->save($data);
        }


        /**
         * @param string $param
         * @param string $order
         * @author: Jackhhy <jackhhy520@qq.com>
         * @name: GetAdminMemberDataPage
         * @describe:获取管理员分页数据
         */
        public function GetAdminMemberDataPage($param = [], $order = "id desc")
        {
            $where = [];
            $limit = 15;
            $page  = 1;
            if(!empty($param)) {
                //添加条件

                //栏目id
                if(isset($param['role_id']) && $param['role_id']!=0) {
                    $where['a.role_id'] = ['eq', $param['role_id']];
                }
                //搜索条件
                if(!empty($param['search'])) {
                    $where['a.username|a.nickname'] = ['like', "%".$param['search']."%"];
                }
                isset($param['limit']) && $param['limit']!=0 ? $limit = (int)$param['limit'] : '';
                isset($param['page']) && $param['page']!=0 ? $page = (int)$param['page'] : 1;
            }
            $field = ('a.*,c.name as role_name');

            try {
                $data = $this->alias('a')->join('admin_role c', 'a.role_id=c.id')->where($where)->order($order)->field($field)->page($page)->limit($limit)->select()->toArray();

                $count = count($data);

                if(!empty($data)) {
                    foreach($data as $k => $v){
                        $data[$k]['pwd'] = UtilService::think_decrypt($v['password']);
                    }
                }

            } catch (Exception $exception) {
                return JsonService::fail($exception->getMessage());
            }
            return JsonService::result(0, "", $data, $count);

        }



        /**
         * @param $id
         * @return int
         * @author: Jackhhy <jackhhy520@qq.com>
         * @name: del
         * @describe:删除
         */
        public function del($id)
        {
            try {
                $del = $this->destroy(['id' => ['in', $id]]);//真实删除
                return $del;
            } catch (Exception $exception) {
                return JsonService::fail($exception->getMessage());
            }
        }



    }