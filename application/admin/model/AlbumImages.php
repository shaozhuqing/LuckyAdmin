<?php
    /**
     * Created by PhpStorm.
     * User: jackhhy
     * Date: 2019/7/10-16:21
     * Link: https://gitee.com/luckygyl/lucky_admin
     * FileName: AlbumImages.php
     * Keys: ctrl+alt+L/ctrl+s(代码格式化) ctrl+J(代码提示) ctrl+R(替换)ALT+INSERT(生成代码(如GET,SET方法,构造函数等) , 光标在类中才生效)
     * CTRL+ALT+O (优化导入的类和包 需要配置) SHIFT+F2(高亮错误或警告快速定位错误)
     * CTRL+SHIFT+Z(代码向前) CTRL+SHIFT+/ (块状注释) ctrl+shift+enter(智能完善代码 如if())
     */

    namespace app\admin\model;


    use app\common\model\ModelBase;
    use service\CacheService;
    use service\JsonService;
    use service\UtilService;
    use think\Exception;

    class AlbumImages extends ModelBase
    {

        /**
         * @param $album_id
         * @return array
         * @throws \think\db\exception\DataNotFoundException
         * @throws \think\db\exception\ModelNotFoundException
         * @throws \think\exception\DbException
         * @author: Jackhhy <jackhhy520@qq.com>
         * @name: getByAlbumId
         * @describe:根据相册ID获取图片
         */
        public function getByAlbumId($album_id)
        {
            return $this->where("album_id", $album_id)->select()->toArray();
        }


        /**
         * @param $data
         * @return array|false
         * @throws \Exception
         * @author: Jackhhy <jackhhy520@qq.com>
         * @name: addData
         * @describe:批量添加数据
         */
        public function addData($data)
        {
            $res = $this->isUpdate(false)->allowField(true)->saveAll($data);
            return $res;
        }


        public function getAll()
        {
            return $this->select()->toArray();
        }


        /**
         * @param $id
         * @return int
         * @author: Jackhhy <jackhhy520@qq.com>
         * @name: del
         * @describe:删除
         */
        public function del($id)
        {
            //dump($id);die;
            try {
                $del = $this->destroy(['id' => ['in', $id]], true);
                if($del) {
                    $data = $this->whereIn("id", $id)->select()->toArray();
                    foreach($data as $k => $v){
                        @unlink($v['img_url']); //删除原图片
                    }
                    CacheService::rm("images_album");

                    $ars = ['describe' => '删除相册图片', 'type' => 2, 'admin_name' => self::$admin_info['username'], 'browse' => UtilService::getBrowser(), 'model' => self::$mode_name, 'controller' => self::$controller_name, 'action' => self::$action_name];
                    AddLogs($ars); //添加操作日志

                    return JsonService::success("删除成功");
                }
                else {
                    return JsonService::fail("删除失败");
                }

            } catch (Exception $exception) {

                return JsonService::fail($exception->getMessage());
            }
        }



    }