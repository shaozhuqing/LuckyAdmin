<?php
    /**
     * Created by PhpStorm.
     * User: jackhhy
     * Date: 2019/7/9-16:31
     * Link: https://gitee.com/luckygyl/lucky_admin
     * FileName: Navigate.php
     * Keys: ctrl+alt+L/ctrl+s(代码格式化) ctrl+J(代码提示) ctrl+R(替换)ALT+INSERT(生成代码(如GET,SET方法,构造函数等) , 光标在类中才生效)
     * CTRL+ALT+O (优化导入的类和包 需要配置) SHIFT+F2(高亮错误或警告快速定位错误)
     * CTRL+SHIFT+Z(代码向前) CTRL+SHIFT+/ (块状注释) ctrl+shift+enter(智能完善代码 如if())
     */

    namespace app\admin\model;


    use app\common\model\ModelBase;
    use service\JsonService;
    use service\StringService;
    use service\TreeService;
    use think\Db;
    use think\Exception;

    class Navigate extends ModelBase
    {


        protected $update = ["update_time"];



        /**
         * @param array $param
         * @param string $order
         * @author: Jackhhy <jackhhy520@qq.com>
         * @name: GetNavigateDataPage
         * @describe:获取栏目分页数据
         */
        public function GetNavigateDataPage($param = [], $order = "listorder asc")
        {
            $where = [];
            $limit = 15;
            $page  = 1;
            if(!empty($param)) {
                //搜索条件
                if(!empty($param['title'])) {
                    $where['title'] = ['like', "%".$param['title']."%"];
                }
                isset($param['limit']) && $param['limit']!=0 ? $limit = (int)$param['limit'] : '';
                isset($param['page']) && $param['page']!=0 ? $page = (int)$param['page'] : 1;
            }

            try {
                $data = $this->where($where)->order($order)->page($page)->limit($limit)->select()->toArray();

                $count = count($data);

                $datas = TreeService::toFormatTree($data); //转换成数结构

            } catch (Exception $exception) {
                return JsonService::fail($exception->getMessage());
            }
            return JsonService::result(0, "", $datas, $count);
        }


        /**
         * @return mixed
         * @author: Jackhhy
         * @name: setUpdateTimeAttr
         * @describe:
         */
        protected function setUpdateTimeAttr()
        {
            return time();
        }

        /**
         * @param $data
         * @return false|int
         * @author: Jackhhy
         * @name: AddData
         * @describe:添加数据
         */
        public function addData($data)
        {
            $res = $this->isUpdate(false)->allowField(true)->save($data);
            return $res;
        }


        /**
         * @param $params
         * @return false|int
         * @author: Jackhhy
         * @name: editData
         * @describe:修改数据
         */
        public function editData($params)
        {
            $res = $this->isUpdate(true)->allowField(true)->save($params);
            return $res;
        }


        /**
         * @return array
         * @throws \think\db\exception\DataNotFoundException
         * @throws \think\db\exception\ModelNotFoundException
         * @throws \think\exception\DbException
         * @author: Jackhhy
         * @name: GetMenuList
         * @describe:获取格式化栏目数据
         */
        public function GetNavigateList($where = [])
        {
            $res = $this->where($where)->order("listorder asc")->select()->toArray();
            if(!empty($res)) {
                foreach($res as $k => $v){
                    $res[$k]['url'] = url($v['href']);
                }
            }
            return $res;
        }



        /**
         * @param $id
         * @return int|void
         * @throws \think\db\exception\DataNotFoundException
         * @throws \think\db\exception\ModelNotFoundException
         * @throws \think\exception\DbException
         * @author: Jackhhy <jackhhy520@qq.com>
         * @name: del
         * @describe:删除
         */
        public function del($id)
        {
            if(strlen($id) > 1) {
                $ids = @explode(',', $id);
                foreach($ids as $v){
                    $resd = $this->findChild($v);//判断是否有子集
                    if($resd!==true) {
                        return JsonService::fail("ID:【".$v."】该栏目下有子集不能删除！");
                    }
                }
            }
            else {
                $res = $this->findChild($id);//判断是否有子集
                if($res!==true) {
                    return JsonService::fail("该栏目下有子集不能删除！");
                }
            }
            try {
                $del = $this->destroy(['id' => ['in', $id]]);
                return $del;
            } catch (Exception $exception) {

                return JsonService::fail($exception->getMessage());
            }


        }



        /**
         * @param $id
         * @return bool
         * @throws \think\db\exception\DataNotFoundException
         * @throws \think\db\exception\ModelNotFoundException
         * @throws \think\exception\DbException
         * @author: Jackhhy <jackhhy520@qq.com>
         * @name: findChild
         * @describe:查找是否有子集
         */
        private function findChild($id)
        {
            $res = TreeService::getChildrenPid($this->GetNavigateList(), $id);
            if(empty($res)) {
                return true;
            }
            else {
                return false;
            }
        }



    }