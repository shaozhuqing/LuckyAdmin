<?php
    /**
     * Created by PhpStorm.
     * User: jackhhy
     * Date: 2019/6/28-11:53
     * Link: https://gitee.com/luckygyl/lucky_admin
     * FileName: ModelBase.php
     * Keys: ctrl+alt+L/ctrl+s(代码格式化) ctrl+J(代码提示) ctrl+R(替换)ALT+INSERT(生成代码(如GET,SET方法,构造函数等) , 光标在类中才生效)
     * CTRL+ALT+O (优化导入的类和包 需要配置) SHIFT+F2(高亮错误或警告快速定位错误)
     * CTRL+SHIFT+Z(代码向前) CTRL+SHIFT+/ (块状注释) ctrl+shift+enter(智能完善代码 如if())
     * 基础模型
     */

    namespace app\common\model;

    use think\Model;
    use think\Db;
    use traits\model\SoftDelete;
    use think\Exception;


    class ModelBase extends Model
    {

        private static $errorMsg;

        private static $transaction = 0;

        private static $DbInstance = [];

        const DEFAULT_ERROR_MSG = '操作失败,请稍候再试!';

        protected $insert=["create_time","time_formt","ip"];


        /**
         * @return mixed
         * @author: Jackhhy
         * @name: setIpAttr
         * @describe:
         */
        protected function setIpAttr()
        {
            return request()->ip();
        }


        /**
         * @return int
         * @author: Jackhhy
         * @name: setCreateTimeAttr
         * @describe:
         */
        public function setTimeFormtAttr(){
            return date("Y-m-d H:i:s",time());
        }
        /**
         * @return int
         * @author: Jackhhy
         * @name: setCreateTimeAttr
         * @describe:
         */
        public function setCreateTimeAttr(){
            return time();
        }


        protected static function getDb($name,$update = false)
        {
            if(isset(self::$DbInstance[$name]) && $update == false)
                return self::$DbInstance[$name];
            else
                return self::$DbInstance[$name] = Db::name($name);
        }

        /**
         * @param string $errorMsg
         * @param bool $rollback
         * @return bool
         * @author: Jackhhy <jackhhy520@qq.com>
         * @name: setErrorInfo
         * @describe:设置错误信息
         */
        protected static function setErrorInfo($errorMsg = self::DEFAULT_ERROR_MSG,$rollback = false)
        {
            if($rollback) self::rollbackTrans();
            self::$errorMsg = $errorMsg;
            return false;
        }


        /**
         * @param string $defaultMsg
         * @return string
         * @author: Jackhhy <jackhhy520@qq.com>
         * @name: getErrorInfo
         * @describe:获取错误信息
         */
        public static function getErrorInfo($defaultMsg = self::DEFAULT_ERROR_MSG)
        {
            return !empty(self::$errorMsg) ? self::$errorMsg : $defaultMsg;
        }

        /**
         * 开启事务
         */
        public static function beginTrans()
        {
            Db::startTrans();
        }

        /**
         * 提交事务
         */
        public static function commitTrans()
        {
            Db::commit();
        }

        /**
         * 关闭事务
         */
        public static function rollbackTrans()
        {
            Db::rollback();
        }



        /**
         * @param $res
         * @author: Jackhhy <jackhhy520@qq.com>
         * @name: checkTrans
         * @describe:根据结果提交滚回事务
         */
        public static function checkTrans($res)
        {
            if($res){
                self::commitTrans();
            }else{
                self::rollbackTrans();
            }
        }





    }