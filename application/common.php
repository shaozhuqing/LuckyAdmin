<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------
/*header('Content-Type:application/json; charset=utf-8');*/



/**
 * @param $str
 * @return mixed
 * 密码隐藏
 */
function hide_pwd($str){
    $resstr = substr_replace($str,'****',3,10);
    return $resstr;
}



    /**
     * @param null $directory
     * @return bool
     * @author: Jackhhy <jackhhy520@qq.com>
     * @name: clear_cache
     * @describe:清除系统缓存
     */
    function clear_cache($directory = null)
    {
        $directory = empty($directory) ? ROOT_PATH . 'runtime/cache/' : $directory;
        if (is_dir($directory) == false) {
            return false;
        }
        $handle = opendir($directory);
        while (($file = readdir($handle)) !== false) {
            if ($file != "." && $file != "..") {
                is_dir($directory . '/' . $file) ?
                    clear_cache($directory . '/' . $file) :
                    unlink($directory . '/' . $file);
            }
        }
        if (readdir($handle) == false) {
            closedir($handle);
            rmdir($directory);
        }
    }


